<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User class.
 * 
 * @extends CI_Controller
 */
class User extends CI_Controller {

	/**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct() {
		
		parent::__construct();
		$this->load->library(array('session'));
        $this->load->library('email');
		$this->load->helper(array('url'));
		$this->load->model('user/user_model');
        $this->load->model('admin/select_acc_model');
        $this->load->model('admin/Logo_model');
		
	}
	
	
	public function index() {
	$data['account'] = $this->select_acc_model->getAllAccounts();	
    //$data['logo'] = $this->Logo_model->getLogo();  
		
	}
   
	
	/**
	 * register function.
	 * 
	 * @access public
	 * @return void
	 */
	public function register() {
		
		// create the data object
		//$data = new stdClass();
		
		// load form helper and validation library
		$this->load->helper('form');
		$this->load->library('form_validation');
         $this->load->model('admin/select_acc_model');
          $data['account'] = $this->select_acc_model->getAllAccounts();
          $data['group'] = $this->select_acc_model->getAllGroups();
        
		
		// set validation rules
		$this->form_validation->set_rules('username', 'Username', 'trim|required|alpha_numeric|min_length[4]|is_unique[users.username]', array('is_unique' => 'This username already exists. Please choose another one.'));
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[users.email]');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]');
		$this->form_validation->set_rules('password_confirm', 'Confirm Password', 'trim|required|min_length[6]|matches[password]');
		
		if ($this->form_validation->run() === false) {
			
			// validation not ok, send validation errors to the view
			//$this->load->view('header');
			$this->load->view('user/register/register', $data);
			//$this->load->view('footer');
			
		} else {
			
			// set variables from the form
			$username = $this->input->post('username');
			$email    = $this->input->post('email');
            
			$password_spec = $this->input->post('password');
            $radom_password = $this->input->post('radom_password');
            $status = $this->input->post('status');
            $account_x = $this->input->post('account');
            $account = implode($account_x,',');
             $group =$this->input->post('group');
            // new 
            if(isset($_REQUEST['organisation'])){
               $organisation = implode(',',$_REQUEST['organisation'] ) ;  
            } else{
                $organisation = '';
            }
             //$organisation = implode(',',$_REQUEST['organisation'] ) ; 
            $location = implode(',',$_REQUEST['location']);
            // end
           
            //$location1 =$_REQUEST['field_name'];
           // $location = $this->input->post('location');;
            if($password_spec != $radom_password){
              $password=$password_spec;  
            } else{
               $password=$radom_password;    
            }
			
			if ($this->user_model->create_user($username, $email, $password,$account,$group,$location,$organisation,$status)) {
				
				// user creation ok
			//	$this->load->view('header');
			//	$this->load->view('user/register/register_success', $data);
           
            
            $this->load->view('admin/register_view', $data);
			//	$this->load->view('footer');
                 $message = "Dear ".$username."\r\n";
                 $message .= "Your account has been created sucessively!\r\n";
                 $message .= "Access your account via: https://csc.mymetajua.com\r\n";
                 $message .= "Username: ".$username."\r\n";
                 $message .= "Passwrod: ".$password."\r\n";
                 $message .= "You are STRONGLY advised to change your password as soon as get log into your account. \r\n";
                 $message .= "Thanks, Metajua | IT Department. \r\n";
                 
                 $config['charset'] = 'utf-8';
                 $config['mailtype'] = 'text';
                 $config['newline'] = "\r\n";
            // Sending email module
                 $this->load->library('email');
                 $this->email->initialize($config);

                $this->email->from('contact@metajua.com', 'IT Department');
                $this->email->to($email);
               // $this->email->cc('another@another-example.com');
                //$this->email->bcc('them@their-example.com');

                $this->email->subject('Metjua-Client Account');
                $this->email->message($message);
               // $this->email->message('Dear '.$username.' , your account has been created sucessively!');
                //$this->email->message('Access your account via: https://gourmet.mymetajua.com');
                //$this->email->message('Username: '.$username);
                //$this->email->message('Passwrod: '.$password);
               // $this->email->message('You are STRONGLY advised to change your password as soon as get into your account');
                
                $this->email->send();
            // end  sending email
				
			} else {
				
				
				$data->error = 'There was a problem creating your new account. Please try again.';
				
				
				$this->load->view('user/register/register', $data);
				
				
			}  
			
		}
		
	}
    // Function that fetch all the organisation name
     public function org_list() {
        $country = implode(',',$this->input->post('location'));
        $user_role = $this->input->post('group'); 
        
        //var_dump($this->select_acc_model->filterGroups($country,$user_role));
        $x= $this->select_acc_model->filterGroups($country,$user_role);
        //var_dump($x);
         echo(json_encode($x) );
              
        
     }
		
	/**
	 * login function.
	 * 
	 * @access public
	 * @return void
	 */
	public function login() {
		
		// create the data object
		//$data = new stdClass();
		
		// load form helper and validation library
		$this->load->helper('form');
		$this->load->library('form_validation');
		
		// set validation rules
		$this->form_validation->set_rules('username', 'Username', 'required|alpha_numeric');
		$this->form_validation->set_rules('password', 'Password', 'required');
		
		if ($this->form_validation->run() == false) {
			
			// validation not ok, send validation errors to the view
		//	$this->load->view('header');
			$this->load->view('user/login/login');
		//	$this->load->view('footer');
			
		} else {
			
			// set variables from the form
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			
			if ($this->user_model->resolve_user_login($username, $password)) {
				
				$user_id = $this->user_model->get_user_id_from_username($username);
				$user    = $this->user_model->get_user($user_id);
                //$user    = $this->user_model->get_user($user_id);
                
				
				// set session user datas
				$data['user']      = (int)$user->id;
				
                $data['account']     = (string)$user->account;
                 
				// user login ok
			//	$this->load->view('header');
			//	$this->load->view('user/login/login_success', $data);
            
                //$this->load->view('test', $data); 
                 $this->load->view('user/accounts/login_acc', $data); 
                 //redirect(site_url('user/accounts/login_acc', $data));
                
			//	$this->load->view('footer');
				
			} else {
				 $data = new stdClass();
				// login failed
				$data->error = 'Wrong username or password.';
				
				// send error to the view
			//	$this->load->view('header');
				$this->load->view('user/login/login', $data);
                 
			//	$this->load->view('footer');
				
			}
			
		}
		
	}
	//
    
    // check accounts
    public function acc_login() {
	    $data = new stdClass();
	      
	    $id = $this->input->post('id');
      	$accounts = $this->input->post('accounts');
       	$user    = $this->user_model->get_new_user($id);
       	$log = $this->user_model->get_logo($accounts);
       
    	        $_SESSION['user']      = (int)$user->id;   
                $_SESSION['username']     = (string)$user->username;
                //New attributes
                $_SESSION['email']     = (string)$user->email;
                $_SESSION['client_id']     = (string)$user->client_id;
                $_SESSION['account']     = $accounts;
                 //$_SESSION['account']     = account;
                 $_SESSION['group']     = (string)$user->groups;
                 $_SESSION['user_type']     = (string)$user->user_type;
                 $_SESSION['language']     = (string)$user->languages;
                  $_SESSION['country']     = (string)$user->country;
                 //Org Prev
                 $_SESSION['organisation_type']     = (string)$user->organisation_type;
                 $_SESSION['orgnisations']     = (string)$user->organisation_restriction;
                 //Location Prev 
                 $_SESSION['delimeter']     = (string)$user->delimeter;
                 $_SESSION['locations']     = (string)$user->location_restriction;
                 //report Prev
                  
                // New attributes
                $_SESSION['logged_in']    = (bool)true;
                $_SESSION['is_confirmed'] = (bool)$user->is_confirmed;
                $_SESSION['is_admin']     = (bool)$user->is_admin;
                
                $group_priv    = $this->user_model->get_user_group($user->groups); 
                  $_SESSION['report']     = (string)$group_priv->name;
                  $_SESSION['view_access']     = (string)$group_priv->view_report;
                  $_SESSION['full_access']     = (string)$group_priv->manage_report;
                  
                 $_SESSION['logo']     = (string)$log->account_logo;
    
    	if($_SESSION['account'] =='KCL'){
       		// $this->load->view('me/ME_compiledlist_view', $data); 
        	redirect(site_url('/admin/sys_dashboard'),$data,'refresh');
    	}else{ 	
       		redirect(site_url('/cws/purchase_summary'),$data,'refresh');
    	}
    }

	/**
	 * logout function.
	 * 
	 * @access public
	 * @return void
	 */
	public function logout() {
		
		// create the data object
		$data = new stdClass();
		
		if (isset($_SESSION['logged_in']) && $_SESSION['logged_in'] === true) {
			
			// remove session datas
			foreach ($_SESSION as $key => $value) {
				unset($_SESSION[$key]);
			}
			
			// user logout ok
		//	$this->load->view('header');
		//	$this->load->view('user/logout/logout_success', $data);
        
       // $this->load->view('user/login/login', $data);
		//	$this->load->view('footer');
        redirect(site_url(),$data,'refresh'); 
			
		} else {
			
			// there user was not logged in, we cannot logged him out,
			// redirect him to site root
			redirect('/');
			
		}
		
	}
    
    public function preset() {
        $this->load->helper('form');
        $this->load->library('form_validation');
        // validations
        $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]');
        $this->form_validation->set_rules('password_confirm', 'Confirm Password', 'trim|required|min_length[6]|matches[password]');
        
        //conditions
         if ($this->form_validation->run() === false) {
            //$data['fail'] = 'Pa'  
            $this->load->view('user/register/password');
           
            
        } else {
            $username = $this->input->post('username');
            $email    = $this->input->post('email');
            $password = $this->input->post('password');
            if ($this->user_model->r_password($username, $email, $password)) {
                $data = new stdClass();
                // remove session datas
                foreach ($_SESSION as $key => $value) {
                    unset($_SESSION[$key]);
                }
                redirect(site_url(),$data,'refresh');
                
                //$this->load->view('user/logout');   
            }
             $data = new stdClass();
                // remove session datas
                foreach ($_SESSION as $key => $value) {
                    unset($_SESSION[$key]);
                }
                redirect(site_url(),$data,'refresh');
           /* else {
                $data['error'] = 'There was a problem resetting your new password. Please try again.'; 
                $this->load->view('user/register/password',$data);
            } */ 
            
           //$data['fail'] = 'Password reset successful'; 
          
        }
        
     
        
    }
    
     public function plost() {
        $this->load->helper('form');
        $this->load->library('form_validation');
        // validations
        $this->form_validation->set_rules('username', 'Username', 'trim|required|alpha_numeric|min_length[4]');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
         //conditions
         if ($this->form_validation->run() === false) {
            //$data['fail'] = 'Pa'  
            $this->load->view('user/lost/plost');
            
            
        } else {
            $username = $this->input->post('username');
            $email    = $this->input->post('email');
           
           $user_id = $this->user_model->search_username($username,$email); 
           if($user_id == 1) {
             $this->load->view('user/login/login');    
           } else{
            $this->load->view('user/lost/plost',$user_id);    
           }
          
        }
       //$this->load->view('user/lost/plost');
        
     }
	
}
