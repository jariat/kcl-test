<?php
  class M_sys_dashboard extends CI_Model
  {
      
      //map coordinates
       public function get_map_points($account)
      {
          $return = array();
         
          $this->db->select('*');
          $this->db->from('organisation_registration');  
          $this->db->where('client_id',$account);
          $this->db->where('instancename','1');    
          $query=$this->db->get();  
          
         // $query = $this->db->get();
          if ($query->num_rows()>0) {
              foreach ($query->result() as $row) {
              array_push($return, $row);
                }
          }   
          return $return;
      }
      
    
      
     
       public function get_farmer_count($account)
      {
         if($_SESSION['organisation_restriction'] !='All'){
          $this->db->select('count(farmercode) farmers');
          $this->db->from('s403_main'); 
          //$this->db->from('kcl_imo_list'); 
          //$this->db->where('client_id',$account);
          $this->db->where('cal_scheme',$_SESSION['organisation_restriction']);
          $year=date('Y');  
          $this->db->where('year',$year); 
          $query=$this->db->get();
        }
        else{
          $this->db->select('count(farmercode) farmers');
          $this->db->from('s403_main'); 
          //$this->db->from('kcl_imo_list'); 
          //$this->db->where('client_id',$account);
          $year=date('Y');  
          $this->db->where('year',$year); 
          $query=$this->db->get();
        }
          //echo $this->db->last_query(); exit;
          return $query->result_array();
      }
      public function get_reg_count($account)
      {
         
          $this->db->select('count(instanceid) regs');
          $this->db->from('farmer_reg_list'); 
          $this->db->where('cal_clientname',$account); 
          $this->db->where('instancename','1');
          $year=date('Y');
          $this->db->where('cal_current_year',$year);
          $query=$this->db->get();
          return $query->result_array();
      }
      public function get_followup_count($account)
      {
         
        if($_SESSION['organisation_restriction'] !='All'){
            $this->db->select('count(instanceid) followup');
            $this->db->from('farmer_audit_followup'); 
            $this->db->where('cal_scheme',$_SESSION['organisation_restriction']); 
            $this->db->where('instancename','1');
            $year=date('Y');
            //$this->db->where('cal_current_year',$year);
            $this->db->where('year',$year);
            $query=$this->db->get();
            return $query->result_array(); 
        }
        else{
          $this->db->select('count(instanceid) followup');
          $this->db->from('farmer_audit_followup'); 
          $this->db->where('instancename','1');
          $year=date('Y');
          //$this->db->where('cal_current_year',$year);
          $this->db->where('year',$year);
          $query=$this->db->get();
          return $query->result_array();
        }
      }
      public function get_org_count($account)
      {
         
          $this->db->select('count(instanceid) orgs');
          $this->db->from('organisation_registration'); 
          $this->db->where('client_id',$account); 
          $this->db->where('instancename','1');
          $query=$this->db->get();
          return $query->result_array();
      }
       
    
  }
?>
